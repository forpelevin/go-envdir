.PHONY: setup
setup: ## Install all the build and lint dependencies
	go get -u github.com/golangci/golangci-lint/cmd/golangci-lint

.PHONY: test
test: ## Run all the tests
	go test -v -race -timeout=30s ./...

.PHONY: fmt
fmt: ## Run goimports on all go files
	go fmt ./...

.PHONY: lint
lint: ## Run all the linters
	golangci-lint run --enable-all --disable="gochecknoglobals,gochecknoinits,scopelint"

.PHONY: build
build: ## Build a version
	go build -v -o go-envdir

# Absolutely awesome: http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := build