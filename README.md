# Go envdir
The CLI app that helps to execute commands using ENV files.
## Getting Started
```
git clone github.com/forPelevin/go-envdir
```
## Prerequisites
For the successful using you should have:
```
go >= 1.12
```
## Running the tests
It's a good practice to run the tests before using the app to make sure everything is OK.
```
cd $GOPATH/src/github.com/forPelevin/go-envdir 
make test
```
## Build the app
```
cd $GOPATH/src/github.com/forPelevin/go-envdir
make build
```
## Sample of using
```
./go-envdir ./test/testdata/envdir env
```
## License
This project is licensed under the MIT License.