package main

import (
	"github.com/forPelevin/go-envdir/pkg/envdir"
	"log"
)

func main() {
	err := envdir.ExecuteWithEnvs()
	if err != nil {
		log.Fatalf("Something went wrong. %v", err)
	}
}
