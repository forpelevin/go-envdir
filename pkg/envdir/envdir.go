// Package envdir provides functions to parse dirs that contain ENV files and call commands
// with the parsed ENVs.
package envdir

import (
	"os"
	"os/exec"
)

// ExecuteWithEnvs reads os.Args, validates them, recursively parses a provided dir with ENV files,
// and call a provided command with the parsed ENV vars. If there is an error on any step the func
// will return it.
func ExecuteWithEnvs() error {
	envDirPath, cmd, err := parseArgs()
	if err != nil {
		return err
	}

	envs, err := parseEnvs(envDirPath)
	if err != nil {
		return err
	}

	return executeWithEnvs(cmd, envs)
}

func executeWithEnvs(cmd string, envs []string) error {
	currDir, err := os.Getwd()
	if err != nil {
		return err
	}

	execCmd := exec.Command(cmd)
	execCmd.Env = envs
	execCmd.Stdout = os.Stdout
	execCmd.Stderr = os.Stderr
	execCmd.Dir = currDir

	return execCmd.Run()
}
