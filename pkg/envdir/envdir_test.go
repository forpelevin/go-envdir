package envdir

import (
	"os"
	"testing"
)

func TestExecuteWithEnvs(t *testing.T) {
	tests := []struct {
		description  string
		envdir       string
		command      string
		expectsError bool
	}{
		{
			description:  "valid envdir and command",
			envdir:       "../../test/testdata/envdir",
			command:      "env",
			expectsError: false,
		},
		{
			description:  "call without envdir and command args",
			expectsError: true,
		},
		{
			description:  "call without the command arg",
			envdir:       "./test/testdata/envdir",
			expectsError: true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.description, func(t *testing.T) {
			os.Args = []string{"main.go", tc.envdir, tc.command}

			err := ExecuteWithEnvs()
			if err != nil && tc.expectsError == true {
				return
			}
			if err != nil {
				t.Fatal(err)
			}
		})
	}
}
