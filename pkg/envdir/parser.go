// Package envdir provides functions to parse dirs that contain ENV files and call commands
// with the parsed ENVs.
package envdir

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
)

// conceivable limit of an ENV value.
const maxEnvVal = 32760

func parseArgs() (envDir string, cmd string, err error) {
	args := os.Args[1:]
	if len(args) == 0 {
		err = errors.New("please specify a valid directory with ENV files and path to an executable command")
		return
	}

	if len(args) == 1 {
		err = errors.New("please specify a valid path to an executable command as second argument")
		return
	}

	return args[0], args[1], nil
}

func parseEnvs(envDir string) ([]string, error) {
	envs := make([]string, 0)
	err := filepath.Walk(envDir, func(path string, f os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		// Here can be a dir but we need only files.
		if f.IsDir() {
			return nil
		}

		value, err := readEnv(path)
		if err != nil {
			return err
		}

		envs = append(envs, fmt.Sprintf("%s=%s", f.Name(), value))

		return nil
	})
	if err != nil {
		return nil, err
	}

	return envs, nil
}

func readEnv(path string) (val string, err error) {
	file, err := os.Open(path)
	if err != nil {
		return
	}
	defer file.Close()

	buf := bytes.NewBuffer(make([]byte, 0, maxEnvVal))
	_, err = io.CopyN(buf, file, maxEnvVal)
	if err != nil && err != io.EOF {
		return
	}

	return buf.String(), nil
}
